import React from "react";
import classes from "./MenuToggle.module.css";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faTimes, faAlignJustify } from "@fortawesome/free-solid-svg-icons";

const MenuToggle = (props) => {
  const cls = [classes.MenuToggle];
  let myIcon = faAlignJustify;

  if (props.isOpen) {
    myIcon = faTimes;
    cls.push(classes.open);
  } else {
    myIcon = faAlignJustify;
    cls.push(classes.close);
  }

  return (
    <FontAwesomeIcon
      className={cls.join(" ")}
      onClick={props.onToggle}
      icon={myIcon}
    />
  );
  // return <i className={cls.join(" ")} onClick={props.onToggle} />;
};

export default MenuToggle;
