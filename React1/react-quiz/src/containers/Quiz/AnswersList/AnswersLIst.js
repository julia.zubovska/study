import React from "react";
import classes from "./AnswersList.module.css";
import AnswerItem from "./AnswerItem/AnswerItem";

//будет получать масив возможных ответов и выводить их на экран
const AnswersList = (props) => {
  return (
    <ul className={classes.AnswersList}>
      {props.answers.map((answer, index) => {
        //возвращаем jsx
        return (
          <AnswerItem
            key={index}
            answer={answer}
            onAnswerClick={props.onAnswerClick}
            //сделаем проверку
            state={props.state ? props.state[answer.id] : null}
          />
        ); //передаем параметр answer с полем text
      })}
    </ul>
  );
};

export default AnswersList;
