import axios from "axios";

export default axios.create({
  baseURL:
    "https://react-quiz-af5d6-default-rtdb.europe-west1.firebasedatabase.app/",
});
