class ApiService {
    constructor(baseUrl) {
        this.url = baseUrl
    }

    async createPost(post) {
        try {
            const request = new Request(this.url + '/posts.json', {
                method: 'post',
                body: JSON.stringify(post)
            })
            return useRequest(request)
        } catch (error) {
            console.log(error)
        }
    }

    async fetchPosts() {
        try {
            const request = new Request(`${this.url}/posts.json`, {
                method: 'get'
            })
            return useRequest(request)
        } catch (error) {

        }
    }

    //будет служить для того что бы загружался определенный пост по id
    async fetchPostById(id) {
        try {
            const request = new Request(`${this.url}/posts/${id}.json`, { //будем грузить нужный нам пост по id
                method: 'get'
            })
            return useRequest(request)
        } catch (error) {

        }
    }
}

async function useRequest(request) {
    const response = await fetch(request)
    return await response.json()
}

export const apiService = new ApiService('https://blog-1095d-default-rtdb.firebaseio.com')