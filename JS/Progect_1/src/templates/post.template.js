export function renderPost(post, options = {}) { //метод будет возвращать верстку 1 поста который будет генирироватся в зависимости данных которые мы передаем
    const tag = post.type === 'news' //является ли пост news тогда пишет так,если не является пишем второе значение 
        ?
        '<li class="tag tag-blue tag-rounded">Новости</li>' :
        '<li class="tag tag tag-rounded">Заметка</li>'

    const favorites = JSON.parse(localStorage.getItem('favorites')) || []
    const candidate = favorites.find(p => p.id === post.id)

    const button = candidate ?
        `<button class="button-round button-small button-danger" data-id="${post.id}" data-title="${post.title}">Удалить</button>` :
        `<button class="button-round button-small button-primary" data-id="${post.id}" data-title="${post.title}">Сохранить</button>`

    return `
        <div class="panel">
        <div class="panel-head">
            <p class="panel-title">${post.title}</p>
            <ul class="tags">
                ${tag}
            </ul>
        </div>
        <div class="panel-body">
            <p class="multi-line">${post.fulltext}</p>
        </div>
        <div class="panel-footer w-panel-footer">
            <small>${post.date}</small>
            ${options.withButton ? button : ''}
        </div>
        </div>
    `
}