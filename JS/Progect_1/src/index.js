import { HeaderComponent } from './components/header.component'
import { NavigationComponent } from './components/navigation.component'
import { FavoriteComponent } from './components/favorite.component'
import { PostsComponent } from './components/posts.component'
import { CreateComponent } from './components/create.component';
import { LoaderComponent } from './components/loader.component';

new HeaderComponent('header')

const loader = new LoaderComponent('loader')

const navigation = new NavigationComponent('navigation')
    //через навигацию манипулируем дивами ниже
const favorite = new FavoriteComponent('favorite', { loader })
const posts = new PostsComponent('posts', { loader })
const create = new CreateComponent('create')


navigation.registerTabs([ //передаем масив из объектов(описываем название и какой компонент нам нужно ассоциировать)
        { name: 'create', component: create },
        { name: 'posts', component: posts },
        { name: 'favorite', component: favorite }
    ]) //регистрация табов