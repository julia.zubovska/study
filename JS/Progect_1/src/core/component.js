export class Component {
    constructor(id) {
        this.$el = document.getElementById(id) //через этот метод получаем определенный элемент
        this.init()
    }

    //метод жизненный цикл компонента, будет вызван после запуска Component
    init() {}

    onShow() {

    }

    onHide() {

    }

    //методы помогут прятать или показывать определенные элементы, нужны что бы каждый раз не писать методы
    hide() {
        this.$el.classList.add('hide')
        this.onHide()
    }

    show() {
        this.$el.classList.remove('hide') //remove удаляет hide
        this.onShow()
    }
}