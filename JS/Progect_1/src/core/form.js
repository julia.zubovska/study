export class Form {
    //container-где содержится сам элемент формы и объект-controls
    constructor(form, controls) {

        this.form = form
        this.controls = controls
            //сохранили в приватных переменных
    }

    //универсальный метод для всех control
    value() { //будет отдавать объект содержащий все необходимые значения
        const value = {}

        //возвращаем массив ключей
        Object.keys(this.controls).forEach(control => {
            value[control] = this.form[control].value
        })


        return value //возвращаем объект
    }


    //очищает формы после нажатия кнопки "создать"
    clear() {
        Object.keys(this.controls).forEach(control => {
            this.form[control].value = ''
        })
    }

    isValid() { //нужно вернуть булевое значение true/fals
        let isFormValid = true //по умолочанию будет валидна

        Object.keys(this.controls).forEach(control => {
            const validators = this.controls[control] //получаем список валидаторов

            let isValid = true
            validators.forEach(validator => {
                isValid = validator(this.form[control].value) && isValid
            })

            // if (!isValid) {  //если не валидная форма тогда устанавлием ошибку
            //     setError(this.form[control])
            // } else {   //если валидная тогда визуально очищаем
            //     clearError(this.form[control])
            // } ниже аналог

            isValid ? clearError(this.form[control]) : setError(this.form[control])

            isFormValid = isFormValid && isValid
        })


        return isFormValid
    }

}

function setError($control) { //независимая функция для отображения ошибки
    clearError($control)

    const error = '<p class="validation-error">Введите текст</p>'
    $control.classList.add('invalid') // что бы подсвечивался красным
    $control.insertAdjacentHTML('afterend', error) //метод позводяет вставлять HTML
        //1-указываем что сразу за эл кладем за input, 2-указываем какой именно эл нужен
}

function clearError($control) { //очищает если до этого уже было указано об ошибке
    $control.classList.remove('invalid')

    if ($control.nextSibling) {
        $control.closest('.form-control').removeChild($control.nextSibling)
    }
}