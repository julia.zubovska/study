export class Validators {
    static required(value = '') { //будет проверять пустой ли control
        return value && value.trim()
    }

    static minLength(length) { //валидатор помогает определить кол-во символов в строке
        return value => {
            return value.length >= length
        }
    }
}