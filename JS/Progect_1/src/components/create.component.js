import { Component } from '../core/component'
import { Form } from '../core/form'
import { Validators } from '../core/validators';
import { apiService } from '../../services/api.services';



export class CreateComponent extends Component {
    constructor(id) {
        super(id)


    }

    //реализуем метод init который позволяет нам инициализировать компоненты и  выполнит какие либо действия после его инициальзации
    init() {
        this.$el.addEventListener('submit', submitHandler.bind(this))

        //будет являтся новым экземпляром форм
        this.hreny = new Form(this.$el, { //в конструктор передаем 2 эл, 1-отвечает за объект формы(весь див create),2-указываем объект
            title: [Validators.required], //мы не вызываем метод, а передаем ссылку
            fulltext: [Validators.required, Validators.minLength(10)]
                //в массиве указываем валидаторы которые нужно применить
        })

    }
}

async function submitHandler(event) {
    event.preventDefault() //что бы при нажатии стр не перезагружалась

    if (this.hreny.isValid()) { //если форма валидная будем делать ниже манипуляцию

        //при сабмите нашей формы необходимо получить все значения формы
        const formData = { //в оюъекте описываем значение которое мы получили в нашей форме которую писали в input
            type: this.$el.type.value,
            date: new Date().toLocaleDateString(), //получаем дату 
            ...this.hreny.value()
        }

        await apiService.createPost(formData)

        this.hreny.clear()
        alert('Запись создана')
    }
}