import { Component } from '../core/component'
import { apiService } from '../../services/api.services';
import { renderPost } from '../templates/post.template';


export class FavoriteComponent extends Component {
    constructor(id, { loader }) {
        super(id)

        this.loader = loader
    }

    init() { //убирает # с адресной строки
        this.$el.addEventListener('click', linkClickHandler.bind(this))
    }

    onShow() { //будем показывать определенный список
        const favorites = JSON.parse(localStorage.getItem('favorites'))
        const html = renderList(favorites) //метод в который мы передали массив favorites
        this.$el.insertAdjacentHTML('afterbegin', html)
    }

    onHide() { //чистим html если когда переключаешь в навигации
        this.$el.innerHTML = ''
    }
}


async function linkClickHandler(event) { //функция где мы будем загружать ссылку
    event.preventDefault()

    //если у эл сласс js-link это означает что мы выполникили правильный клик
    if (event.target.classList.contains('js-link')) {
        const postId = event.target.dataset.id

        this.$el.innerHTML = '' // чтобы очистить компонент
        this.loader.show()
        const post = await apiService.fetchPostById(postId)
        this.loader.hide()

        this.$el.insertAdjacentHTML('afterbegin', renderPost(post, { withButton: false }))

    }
}


function renderList(list = []) {
    if (list.length) { //если что то есть в листе то будем выводить сами лист
        console.log(list)
        return `
            <ul>
                ${list.map(i => `<li><a href="#" class="js-link" data-id="${i.id}">${i.title}</a></li>`).join(' ')}
            </ul>
        `
    }

    return `<p class="center">Вы ничего не добавили</p>` //иначе будем вызывать сроку где ничего нет
}