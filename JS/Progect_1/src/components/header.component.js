import { Component } from '../core/component'

export class HeaderComponent extends Component {
    //что бы наслодевалось от Component нужно вызвать constructor и super и мы получаем приватную переменную this.$el
    constructor(id) {
        super(id)
    }

    init() {
        //если юзер посетил стр по умолочанию будем скрывать первую стр приветствия
        if (localStorage.getItem('visited')) {
            this.hide()
        }
        this.$el.querySelector('.js-header-start').addEventListener('click', buttonHandler.bind(this))
    }
}

//при нажатии на кнопку будет скрыватся header
function buttonHandler() {
    localStorage.setItem('visited', JSON.stringify(true)) //позволяет понять был ли юзер на стр
    this.hide()
}