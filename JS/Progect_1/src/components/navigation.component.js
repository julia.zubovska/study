import { Component } from '../core/component'

export class NavigationComponent extends Component {
    constructor(id) {
        super(id)

        //дает доступ к манипуляции формами(create, favorite, posts)
        this.tabs = []
    }

    init() {
        this.$el.addEventListener('click', tabClickHandler.bind(this))
    }

    //отдельный публичный метод
    registerTabs(tabs) {
        this.tabs = tabs
    }
}

function tabClickHandler(event) {
    event.preventDefault() //убрали # с адресной строки
    if (event.target.classList.contains('tab')) {
        Array.from(this.$el.querySelectorAll('.tab')).forEach(tab => { //очищает предыдущий таб от все классов
            tab.classList.remove('active')
        })

        event.target.classList.add('active') //добавляет class active тому табу на который мы нажимаем


        //связываем навигацию с формами
        const activeTab = this.tabs.find(t => t.name === event.target.dataset.name)

        //cкрываем все элементы которые не относятся к active tab и показывать текущий
        this.tabs.forEach(t => t.component.hide())
        activeTab.component.show()
            //так же это объкт и у него есть поле сomponent
    }
}