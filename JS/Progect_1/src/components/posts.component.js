import { Component } from '../core/component'
import { apiService } from '../../services/api.services';
import { transformServices } from '../../services/transform.services';
import { renderPost } from '../templates/post.template';



export class PostsComponent extends Component {
    constructor(id, { loader }) {
        super(id)
        this.loader = loader
    }

    init() {
        this.$el.addEventListener('click', buttonHandler.bind(this))
    }

    async onShow() {
        this.loader.show()

        const fbData = await apiService.fetchPosts()
        const posts = transformServices.fbObjectToArray(fbData)
        const html = posts.map(post => renderPost(post, { withButton: true }))

        this.loader.hide()

        this.$el.insertAdjacentHTML('afterbegin', html.join(' '))
    }

    //очищаем html(форму) когда переключаемся на дугую вкладку в навигации
    onHide() {
        this.$el.innerHTML = ''
    }
}


//функция помогает при нажатии на кнопку перенести пост в избранные
function buttonHandler(event) {
    const $el = event.target
    const id = $el.dataset.id
    const title = $el.dataset.title

    if (id) {
        let favorites = JSON.parse(localStorage.getItem('favorites')) || []
        const candidate = favorites.find(p => p.id === id)

        //проверяем если ли в данном массиве id
        if (candidate) { //если есть то удаляем
            $el.textContent = 'Сохранить' //мы его удалили и меняем кнопку на Сохранить
            $el.classList.add('button-primary')
            $el.classList.remove('button-danger')

            favorites = favorites.filter(p => p.id !== id)
        } else { //если нет то добавляеI
            $el.textContent = 'Удалить'
            $el.classList.remove('button-primary')
            $el.classList.add('button-danger')

            favorites.push({ id, title })
        }

        localStorage.setItem('favorites', JSON.stringify(favorites))
    }
}