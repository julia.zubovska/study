//в переменную через функцию нода require кладем содержимое пакета request-promise
const { json } = require('body-parser')
const rq = require('request-promise')

//экспорт функции на ружу
module.exports = async function(city = '') {
    if (!city) {
        throw new Error('Имя города не может быть пустым')
    }

    const KEY = 'd67492dda438db3db5efb5b8acc72a16'
    const uri = 'http://api.openweathermap.org/data/2.5/weather?q={city name}&appid={API key}'

    const options = {
        uri,
        qs: {
            appid: KEY,
            q: city,
            units: 'imperial'
        },
        json: true
    }

    try {
        const data = await rq(options) //сделали запрос

        const celsius = (data.main.temp - 32) * 5 / 9

        return {
            weather: `${data.name}: ${celsius.toFixed(0)}`,
            error: null
        }
    } catch (error) {
        return {
            weather: null,
            error: error.error.message
        }
    }


}