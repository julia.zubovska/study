const express = require('express') //подключаем пакет к Js через require получаем доступ
const bodyParser = require('body-parser')

const app = express() //переменная отвечает за все приложение и определяется результатом функции express

const weatherRequest = require('./requests/weatherRequest')

console.log(weatherRequest)


app.set('view engine', 'ejs')
app.use(express.static('public'))
app.use(bodyParser.urlencoded({ extended: true }))

//код позволит отреагировать на запрос пользователя
app.get('/', (req, res) => {
    res.render('index', { weather: null, error: null })
})


//возвращаем страницу с погодой
app.post('/', async(req, res) => {
    const { city } = req.body

    const { weather, error } = await weatherRequest(city)
    res.render('index', { weather, error })
})



//1-порт на котором мы будем слушать приложение-3000 и 2-коллбэк
const port = 3001
app.listen(port, () => {
    console.log(`Server has started on port ${port}...`)
})