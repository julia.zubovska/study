const { Schema, model } = require('mongoose')

const userSchema = new Schema({
  email: {
    type: String,
    required: true,
  },
  name: String,
  password: {
    type: String,
    required: true,
  },
  resetToken: String,
  resetTokenExp: Date,
  cart: {
    items: [
      {
        count: {
          type: Number,
          required: true,
          default: 1,
        },
        courseId: {
          type: Schema.Types.ObjectId,
          ref: 'Course',
          required: true,
        },
      },
    ],
  },
})

//addToCart-метод, который мы определяем как функцию,тк мы будем работать с this
userSchema.statics.addToCart = function (course, user) {
  const items = [...user.cart.items]
  console.log(items)

  const idx = items.findIndex((c) => {
    return c.courseId.toString() === course._id.toString()
  })

  if (idx >= 0) {
    items[idx].count = items[idx].count + 1
  } else {
    items.push({
      courseId: course._id,
      count: 1,
    })
  }

  this.cart = { items }
  return this.save()
}

userSchema.method.removeFromCart = function (id) {
  let items = [...this.cart.items]
  const idx = items.findIndex((c) => {
    return c.courseId.toString() === id.toString()
  })

  if (items[idx].count === 1) {
    items = items.filter((c) => {
      return c.courseId.toString() !== id.toString()
    })
  } else {
    items[idx].count--
  }

  this.cart = { items }
  return this.save()
}

userSchema.methods.clearCart = function () {
  this.cart = { items: [] }
  return this.save()
}

module.exports = model('User', userSchema)
