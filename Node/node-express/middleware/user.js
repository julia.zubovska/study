const User = require('../modules/user')

module.exports = async function (req, res, next) {
  if (!req.session.user) {
    return next()
  }

  req.user = User.findById(req.session.user._id)
  next()
}
