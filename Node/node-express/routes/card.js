const { Router } = require('express')
const Course = require('../modules/course')
const User = require('../modules/user')
const auth = require('../middleware/auth')
const router = Router()

function mapCartItems(items) {
  return items.map((c) => ({
    ...c.courseId,
    count: c.count,
  }))
}

function computePrice(courses) {
  return courses.reduce((total, course) => {
    return (total += course.price * course.count)
  }, 0)
}

router.post('/add', auth, async (req, res) => {
  const course = await Course.findById(req.body.id).lean().exec()
  const user = await req.user.exec()

  const items = [...user.cart.items]

  const idx = items.findIndex((c) => {
    return c.courseId.toString() === course._id.toString()
  })

  if (idx >= 0) {
    items[idx].count = items[idx].count + 1
  } else {
    items.push({
      courseId: course._id,
      count: 1,
    })
  }

  await User.findOneAndUpdate(
    { _id: user._id },
    { $set: { 'cart.items': items } }
  )
    .lean()
    .exec()
  // await User.addToCart(course, user)
  res.redirect('/card')
})

router.delete('/remove/:id', auth, async (req, res) => {
  const course = await Course.findById(req.params.id).lean().exec()
  const user = await User.findOne({ _id: course.userId }).lean().exec()

  let items = [...user.cart.items]
  const idx = items.findIndex((c) => {
    return c.courseId.toString() === course._id.toString()
  })

  if (items[idx].count === 1) {
    items = items.filter((c) => {
      return c.courseId.toString() !== course._id.toString()
    })
  } else {
    items[idx].count--
  }

  const newCart = await User.findOneAndUpdate(
    { _id: course.userId },
    { $set: { 'cart.items': items } },
    { new: true }
  )
    .lean()
    .exec()

  const courses = mapCartItems(newCart.cart.items)

  res.render('card', {
    title: 'Корзина',
    isCard: true,
    courses: courses,
    price: computePrice(courses),
  })
})

router.get('/', auth, async (req, res) => {
  const user = await req.user.populate('cart.items.courseId').lean().exec()

  const courses = mapCartItems(user.cart.items)

  res.render('card', {
    title: 'Корзина',
    isCard: true,
    courses: courses,
    price: computePrice(courses),
  })
})

module.exports = router
