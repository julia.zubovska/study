const { Router } = require('express')
const Order = require('../modules/order')
const User = require('../modules/user')
const auth = require('../middleware/auth')
const router = Router()

router.get('/', auth, async (req, res) => {
  try {
    const user = await req.user.exec()

    const orders = await Order.find({
      'user.userId': user._id,
    })
      .populate('user.userId')
      .lean()
      .exec()

    res.render('orders', {
      isOrder: true,
      title: 'Заказы',
      orders: orders.map((o) => {
        return {
          ...o,
          price: o.courses.reduce((total, c) => {
            return (total += c.count * c.course.price)
          }, 0),
        }
      }),
    })
  } catch (e) {
    console.log(e)
  }
})

router.post('/', auth, async (req, res) => {
  try {
    const user = await req.user.populate('cart.items.courseId').lean().exec()

    const courses = user.cart.items.map((i) => ({
      count: i.count,
      course: { ...i.courseId },
      price: i.price,
    }))

    const order = new Order({
      user: {
        name: user.name,
        userId: user._id,
      },
      courses: courses,
    })

    await order.save()
    await User.findOneAndUpdate(
      { _id: user._id },
      { cart: { items: [] } }
    ).exec()
    res.redirect('/orders')
  } catch (e) {
    console.log(e)
  }
})

module.exports = router
