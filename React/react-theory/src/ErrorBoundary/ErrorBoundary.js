import React from "react";

export default class ErrorBoundary extends React.Component {
  state = {
    hasError: false,
  };

  componentDidCatch(error, info) {
    //будет вызван в том случае если его дети словили exception(ошибки)
    this.setState({ hasError: true });
  }

  render() {
    if (this.state.hasError) {
      //если есть ошибка
      return <h1 style={{ color: "red" }}>Something went wrong</h1>;
    }
    return this.props.children; //если нет ошибки
  }
}
