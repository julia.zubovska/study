import { Component, React } from "react";

const withClass = (Component, className) => {
  //вернем новый компонент-новую функцию
  return (props) => {
    return (
      <div className={className}>
        <Component {...props} />
      </div>
    );
  };
}; //будет добавлять класс корневому элементу и далее будет рендерить компонент который мы передадим

export default withClass;
