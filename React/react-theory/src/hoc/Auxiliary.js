import React from "react";

const Auxiliary = (props) => {
  return props.children;
};

export default Auxiliary;

//идея - мы создаем реакт компонент и он только возврашает props children
