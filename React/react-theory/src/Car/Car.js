import React from "react";
import "./Car.css";
import withClass from "../hoc/withClass";

class Car extends React.Component {
  render() {
    const inputClasses = ["input"]; //без точки, название класса
    if (this.props.name !== "") {
      //если props.name не равна пустой строке
      inputClasses.push("green"); //тогда добавляем inputClasses(ему) с помощью-push класс-green
    } else {
      inputClasses.push("red"); //иначе будет red
    }

    if (this.props.name.lenght > 4) {
      //если в строке будет больше 4 символов
      inputClasses.push("bold");
    }

    return (
      <React.Fragment>
        <h3>Car name: {this.props.name}</h3>
        <p>
          Year: <strong>{this.props.year}</strong>
        </p>
        <input
          type="text"
          onChange={this.props.onChangeName}
          value={this.props.name}
          className={inputClasses.join(" ")}
        />
        {/* в className нужно передавать только строку и с помощью-join массив-inputClasses превращается в строку */}

        <button onClick={this.props.onDelete}>Delete</button>
      </React.Fragment>
    );
  }
}

export default withClass(Car);
