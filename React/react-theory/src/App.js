import { Component } from "react";
import "./App.css";
import Car from "./Car/Car";
import ErrorBoundary from "./ErrorBoundary/ErrorBoundary";
import Counter from "./Counter/Counter";

class App extends Component {
  state = {
    cars: [
      { name: "Ford", year: 2018 },
      // { name: "Audi", year: 2020 },
      // { name: "Mazda", year: 2010 },
    ],
    pageTitle: "React components",
    showCars: false,
  };

  toggleCarsHandler = () => {
    //метод который срабатывает после клика на кнопку

    this.setState({
      showCars: !this.state.showCars,
    }); //в () передаем новое сосотояние
  };

  onChangeName(name, index) {
    const car = this.state.cars[index];
    car.name = name;

    const cars = [...this.state.cars]; //развернули все эл данного массива в нутри нового массива
    cars[index] = car;
    this.setState({ cars }); //изменили состояние
  }

  deleteHandler(index) {
    const cars = this.state.cars.concat(); //создаст новую копию массива
    cars.splice(index, 1);

    this.setState({ cars });
  }

  componentWillMount() {
    console.log("App componentWillMount");
  }

  componentDidMount() {
    console.log("App componentDidMount");
  }

  render() {
    console.log("App render");

    const divStyle = {
      textAlign: "center",
    };

    let cars = null;

    if (this.state.showCars) {
      cars = this.state.cars.map((car, index) => {
        return (
          <ErrorBoundary key={index}>
            <Car
              name={car.name}
              year={car.year}
              onDelete={this.deleteHandler.bind(this, index)}
              onChangeName={(event) =>
                this.onChangeName(event.target.value, index)
              }
            />{" "}
          </ErrorBoundary>
        );
      });
    }

    return (
      <div style={divStyle}>
        <h1>{this.state.pageTitle}</h1>

        <Counter />

        <button style={{ marginTop: 20 }} onClick={this.toggleCarsHandler}>
          Toggle cars
        </button>

        <div
          style={{
            width: 400,
            margin: "auto",
            paddingTop: "20px",
          }}
        >
          {cars}
        </div>
      </div>
    );
  }
}

export default App;
